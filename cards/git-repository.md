# git repository

## Start the daemon

Assuming `srv` contains repositories `foo` and `bar`.

```
cd srv
touch cfg
GIT_CONFIG_GLOBAL=$PWD/cfg git daemon --reuseaddr --base-path=. --export-all --informative-errors --verbose --enable=receive-pack
```

## Clone repositories

```
git clone git://localhost/foo
```
