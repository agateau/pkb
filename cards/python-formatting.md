# Python formatting

Formatting floats with 2 digits after the dot:

    f"{var:.2f}"

Left align strings:

    f"{var:<50}"

Right align strings:

    f"{var:>50}"
