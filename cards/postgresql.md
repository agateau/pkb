# postgresql

## Configuration de pg_hba.conf

```
# Accès pour le user postgres
local   all         postgres                                        ident sameuser

# Accès des users en utilisant les pass postgres
local   all         all                                             md5
host    all         all         127.0.0.1         255.255.255.255   md5

# Désactive le reste
host    all         all         0.0.0.0           0.0.0.0           reject
```

! L'ordre des entrées est importantes !

## Créer un compte

```
sudo su - postgres
createuser -P $user
```

## Créer une base pour un user

```
sudo su - postgres
createdb -O $user $db
```

## Se connecter

```
psql $db $user
```

## Autoriser un user à créer une base

```
sudo su - postgres
psql postgres
alter user $user createdb;
```

## Problèmes après mise à jour

### La base de données n'est pas visible depuis psql:

Raison : L'ancienne version de postgresql fonctionne encore et c'est elle qui contient les données.

Solution :

- Faire un tarball de /var/lib/postgresql au cas où.

- Installer les paquets client et serveur de l'ancienne version depuis <https://wiki.postgresql.org/wiki/Apt>

- Dumper la base en utilisant l'ancien pg_dump

- Arrêter postgresql. Vérifier que tous les exécutables sont arrêtés (anciens et nouveaux !)

- Redémarrer postgresql

- Restaurer la base

### Django ne se connecte pas

Vérifier que postgresql est configuré pour utiliser le port par défaut (5432).

<http://efod.se/blog/archive/2010/02/06/pscopg2-operationalerror>
