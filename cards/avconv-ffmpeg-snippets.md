# avconv/ffmpeg snippets

## Copy audio, change video to x264

    ffmpeg -i $file -acodec copy -vcodec libx264 out/${file%.avi}.mp4

## Copy video, change audio to vorbis

    ffmpeg -i fort_boyard_2013-08-10_20-45.mkv -vcodec copy -acodec libvorbis fb.mkv

## Encode a TV record

    ffmpeg -i tvshow.m2t -acodec libmp3lame -vcodec libx264 -r 25 -s 960x540 tvshow.mp4

## Crop a video rectangle

    mplayer -vf cropdetect file.video
    ffmpeg -i file.video -vf crop=W:H:X:Y ...

## Extract part of a video

    ffmpeg -ss hh:mm:ss[.xxx] -i file.video -t hh:mm:ss[.xxx] ...

## Resize a video

    ffmpeg -i $input -acodec copy -vcodec libx264 -s ${width}x${height} $output.mp4

or

    ffmpeg -i $input -acodec libvorbis -vcodec vp8 -s ${width}x${height} $output.webm
