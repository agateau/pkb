# git temporarily ignore some changes

## Ignore the changes

```
git update-index --assume-unchanged <files>
```

## Revert

```
git update-index --no-assume-unchanged <files>
```
