# How to solve a design problem

## Research

- Define target user
- Understand the need
- Bench-marketing (?)

## Concept

- Brainstorm user journeys
- Identify the ones to follow

## Define Experience

- Attraction: Attract the user to the feature
- Action: Have the feature meet user goals
- Closer: Finish the experience and guarantee a return

## Validation

- Users test to identify the right path to follow

## Design Development

- Integrate test findings
- Solve edge user journey
