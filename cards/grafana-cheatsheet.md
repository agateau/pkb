# grafana cheatsheet

## Creating a time series chart

- Select time series in the top-right drop-down
- Group by "Date Histogram", "@timestamp"
- Group by something else, don't forget to change "Order By" to "Count"
