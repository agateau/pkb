# Discover which tests exercise the code (python)

coverage can help you answer the question: "Which tests exercise this code?"

To do so:

- edit your `pyproject.toml` file to add `dynamic_context = "test_function"` to the `tool.coverage.run` section
- run coverage html with the `--show-context` option

The produced HTML now provides collapsible sections showing test calls.
