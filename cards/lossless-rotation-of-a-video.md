# Lossless rotation of a video

Use exiftool

Read rotation:

```
$ exiftool myvideo.mp4
(...)
Avg Bitrate  : 1.58 Mbps
Rotation     : 90
```

Write rotation:

```
exiftool -rotation=[0|90|180|190] myvideo.mp4
```
