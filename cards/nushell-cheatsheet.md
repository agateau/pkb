# nushell cheatsheet

## Edit PATH

```
$env.PATH = ($env.PATH | prepend /some/path)
```

or:

```
$env.PATH = ($env.PATH | append /some/path)
```

## List all detectors with ContextWindowBanlistPostValidator

```
ls **/*.yaml | get name | each {|x| open $x } | select name post_validators -i | where post_validators != null | flatten | where post_validators.type == "ContextWindowBanlistPostValidator" | flatten | select name window_width window_direction patterns -i | flatten
```

## Filter out empty cells

```
| where field_name != null
```

## Update a field

```
| update foo {|e| $e.bar + 2 }
```

## Replace symlinks with their file

```
ls */* -l | where type == symlink | select name target | each {|x| rm $x.name ; cp $x.target $x.name }
```

## Filter a list (not a table)

The variable for the iteration is `$it`:

```
git branch -r | lines | where $it =~ auto-update
```
