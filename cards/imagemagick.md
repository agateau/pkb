# Imagemagick

## Smoothly resize an image

```
convert -sample $SIZE $INPUT $OUTPUT
```

See below for possible `$SIZE` values.

## Resize an image, keeping pixels intact

```
convert -scale $SIZE $INPUT $OUTPUT
```

See below for possible `$SIZE` values.

## Possible `$SIZE` values

`$SIZE` can be:

- a percentage. For example `"400%"`.
- `$WIDTH`: preserve aspect ratio.
- `x$HEIGHT`: preserve aspect ratio.
- `${WIDTH}x${HEIGHT}`: maximum size, preserve aspect ratio.
- `${WIDTH}x${HEIGHT}^`: minimum size, preserve aspect ratio.
- `${WIDTH}x${HEIGHT}!`: forced size.
- `${WIDTH}x${HEIGHT}<`: shrink if larger, preserve aspect ratio.
- `${WIDTH}x${HEIGHT}>`: enlarge if smaller, preserve aspect ratio.

More at <https://www.imagemagick.org/script/command-line-processing.php#geometry>.
