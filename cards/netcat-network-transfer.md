# netcat network transfer

## Receiver

### Old nc

```
nc -l -p 1234 | tar -xf -
```

### New nc

```
nc -l 1234 | tar -xf -
```

## Sender

```
tar c $dir | nc $host 1234
```
