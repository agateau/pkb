# lowercase-uppercase

## French

### Months & days

Lower-case except at start of sentence.

## English

### Languages

Capitalized as noon and adjectives.
<https://www.grammarly.com/blog/capitalization-countries-nationalities-languages/>

### Months

Upper-case.
