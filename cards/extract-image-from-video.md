# Extract image from video

- `-ss`: position

- `-y`: force overwrite

```
ffmpeg -i out_0001.mkv -ss 00:00:02.850 -frames 1 -y out.png
```
