# Socks tunnel

## Setup proxy

```
ssh -N -D 9090 agc
```

or

```
ssh -N -D 9090 gi

```

## Firefox

- Open preferences
- Scroll to "Network Settings"
- Click on "Settings..."
- Configure like this:
    - Manual proxy configuration
    - SOCKS Host: 127.0.0.1 Port: 9090
    - SOCKS v5
    - No Proxy for: localhost, 127.0.0.1
    - Proxy DNS when using SOCKS v5

Check: Visit <https://www.myip.com/>

## Ref

<https://linuxize.com/post/how-to-setup-ssh-socks-tunnel-for-private-browsing/>
