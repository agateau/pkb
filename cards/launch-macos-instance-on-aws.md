# Launch macOS instance on AWS

## Create instance

1. Create a "dedicated host" of the right type
2. Create the instance, check the following in "Advanced details":
    - Set "Tenancy" to "Dedicated host"
    - Set "Target host by" to "Host ID" (option appears when ↑ is done)
    - Set "Tenancy host ID" to the dedicated host created at step 1 (option appears when ↑ is done)

## Connect using SSH

To connect, use username `ec2-user`

## Connect using VNC

```
sudo defaults write /var/db/launchd.db/com.apple.launchd/overrides.plist com.apple.screensharing -dict Disabled -bool false
sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.screensharing.plist
```

Set password:

```
sudo /usr/bin/dscl . -passwd /Users/ec2-user
```

Setup tunnel

```
ssh -i <path/to/privatekey> -L 5900:localhost:5900 ec2-user@<public_ip>
```
