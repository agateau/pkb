# Print poster

- Export as PNG with Inkscape, without page margins
- Split PNG with posterazor, no overlap
- Print with Okular. Printer properties:
    - "Page" tab: Page size: A4, margins: 10mm for all sides
    - "Advanced" tab: Print Quality: Best
