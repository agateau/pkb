# Create offline doc for Zeal

## Install doc2dash

    pipx install doc2dash

## Build the doc

If there is a doc target in tox.ini: `tox -e doc`

Look at readthedoc.yaml or .readthedoc.yaml

More ideas: <https://hynek.me/articles/productive-fruit-fly-programmer/>

## Convert the doc

    doc2dash --icon path/to/small/icon.png path/to/html

Should produce $product.docset

## Install the doc

    mv $product.docset $HOME/.local/share/Zeal/Zeal/docsets/

Restart Zeal.
