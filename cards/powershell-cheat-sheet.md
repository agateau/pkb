# Powershell cheat sheet

## Environment variables

### Print variable

    $env:FOO

### Set variable

    $env:FOO="value"

`"` are required!

## `which` equivalent

    gcm <command>

or:

    Get-Command <command>
