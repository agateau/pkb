# Cooking cheatsheet

- Oignons qui piquent : aiguiser couteau
- Volume de sel : 1% du poids du plat
- Four à convection : grille plus vite en surface → bien pour les légumes grillés
- Pâtes :
    - mettre beaucoup d'eau pour que ça continue de bouillir quand on ajoute les pâtes
    - saler l'eau
- Œufs brouillés : cuisson lente (3 minutes), avec beaucoup de beurre
