# Setup a server

## Packages to install

```
apt-get install etckeeper screen htop vim zsh git tig python sudo exuberant-ctags
```

## Create user

```
useradd agateau
chsh agateau /bin/zsh
```

## Setup account

git clone etc repo

```
mkdir .config
cd etc
./ln-dot-files
```

### Setup screen-based shell

```
echo 'exec screen -d -R' > .zprofile
```
