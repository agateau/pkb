# Extract image info

## JPEG

```
exiv2 -pv $file
```

## PNG

```
pnginfo $file
```
