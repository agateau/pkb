# bzr

## clone, get and checkout

```
bzr clone => Standalone tree (unbound)
bzr get == bzr clone
bzr checkout => Checkout of branch (bound)
```

## Basic workflow

```
bzr clone <repo>
bzr add <file>
dch -i
bzr diff
bzr commit
bzr push bzr+ssh://agateau@bazaar.launchpad.net/~agateau/kubuntu-default-settings/<branchname>
```

## Patching a KDE package

```
cd <basedir>
apt-get source <package>
bzr clone lp:~kubuntu-members/<package>/ubuntu <package>
cd <package>-<version>
rm -rf debian
cp -r ../<package>/debian .
Hack
cd debian
rsync -av changelog control rules patches *.install *.pre* *.post* --exclude '*.debhelper' *.lintian-overrides ../../<package>/debian
cd ../../<package>
bzr commit
bzr push bzr+ssh://agateau@bazaar.launchpad.net/~agateau/<package>/<branchname>
```

## Packaging a new version

```
bzr clone <ubuntu branch>
cd <ubuntu branch checkout>
bzr merge <new branch>
bzr ci
dch -i
```

## Creating a package

```
cd <packaging/dir>
mkdir <name>
cd <name>
cp <build/dir>/<name>-<version>.tar.bz2 <name>-<version>.orig.tar.bz2
cd <name>-<version>
dh_make
edit changelog => x.y-1~ag1
debuild
debuild -S -sa
cd ..
dput <ppa> <name>...source.changes
```

## Upgrade to a new repository format

```
bzr upgrade
```
