- Ctrl+Q: shutdown, again to force
- A,S,D: Increase upload throttle by 1, 5 ,50 KB
- Z,X,C: Decrease upload throttle by 1, 5 ,50 KB
- Shift+A,S,D: Increase download throttle by 1, 5 ,50 KB
- Shift+Z,X,C: Decrease download throttle by 1, 5 ,50 KB
- Backspace: add torrent via URL or path

## Views

- 1: All downloads
- 2: All downloads, sorted by name
- 3: Started downloads
- 4: Stopped downloads
- 5: Completed downloads
- 6: Incomplete downloads
- 7: Hashing downloads
- 8: Seeding downloads

## Download view

- u: tracker list
- i: file list
- o: torrent info
- p: peer list
