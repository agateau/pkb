# Install Qt with aqt

    pip install aqtinstall
    mkdir /opt/Qt$VERSION
    cd /opt/Qt$VERSION
    aqt install $VERSION linux desktop gcc_64
    aqt doc $VERSION linux desktop
