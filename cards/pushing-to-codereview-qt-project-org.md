# Pushing to codereview.qt-project.org

## Initial push

- check changeid is in commit msg:

```
git log HEAD
```

- push
    - Qt4: `git push qtproj HEAD:refs/for/4.8`
    - Qt5: `git push qtproj HEAD:refs/for/dev`

## Update

- fix code
- commit with `git ci --amend` (fiddle with Change-Id?)

```
git push qtproj HEAD:refs/for/dev
```

<http://wiki.qt-project.org/Gerrit_Introduction>
