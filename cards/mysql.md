# mysql

## Définir le mot de passe root

```
mysql -u root

-- Connexion locale
set password for 'root'@'localhost' = password('$password');

-- Connexion depuis $hostname (utiliser % pour $hostname pour se connecter de partout)
set password for 'root'@'$hostname' = password('$password');

-- Si 'root'@'$hostname' n'existe pas
create user 'root'@'$hostname' identified by '$password';
grant all on *.* to 'root'@'$hostname' with grant option;
```

## Créer un user

```
create user '$user'@'$hostname' identified by '$password';
```

## Créer une base

```
mysql -u root -p
create database $db;
grant all on $db.* to $user;
```

## Se connecter

```
mysql $user -p $db
```

## Dumper la base

```
mysqldump -u root -p $db > $db.sql
```
