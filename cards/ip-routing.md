# IP Routing

```
sudo -s
modprobe ipt_MASQUERADE
iptables -F; iptables -t nat -F; iptables -t mangle -F
```

Find the name of the network interface connected to the Internet (Something like `wlp3s0`).

```
iptables -t nat -A POSTROUTING -o $INTERNET_NIC -j MASQUERADE
echo 1 > /proc/sys/net/ipv4/ip_forward
```

Copied from <https://tldp.org/HOWTO/Masquerading-Simple-HOWTO/summary.html>.
