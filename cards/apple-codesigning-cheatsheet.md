# Apple codesigning cheatsheet

## Verify signature of a binary

```
codesign -dv -v path/to/binary
```

## List available certificates

```
security find-identity -p basic -v
```
