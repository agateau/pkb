# Secure SSMTP install

apt-get install ssmtp

useradd -g nogroup -s /bin/false -d /nonexistent -c "sSMTP pseudo-user

cd /etc/ssmtp

chown -R ssmtp .

chmod 640 *

chmod 4750 .

chown ssmtp:root /usr/sbin/ssmtp
chmod 4555 /usr/sbin/ssmtp
