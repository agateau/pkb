# Elasticsearch query cheatsheet

## Filter on ranges

    date:[2021-04-02T12:50:31.491Z TO 2021-04-03T12:50:31.491Z]

or:

    date:[2021-04-02 TO 2021-04-03]

or:

    date:>2021-09-20

or:

    elapsed:[2.0 TO *]

## IN query

    file.file_extension:("env" "py")

Quotes are required!

## Negative query

    NOT foo:bar

## Regular expressions

    matches.apikey.string_matched.keyword:/the_regex/

Limitations:

- does NOT support `^` and `$`
- regex must match the whole string

Supported syntax reference: <https://www.elastic.co/guide/en/elasticsearch/reference/current/regexp-syntax.html>
