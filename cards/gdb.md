# gdb

## Breakpoints

Poser un breakpoint :

```
break [emplacement] (=b)
```

Lister les breakpoints :

```
info breakpoints (=i b)
```

Désactiver un breakpoint :

```
disable [numero]
```

Réactiver un breakpoint :

```
enable [numero]
```

## Breakpoints avancés

Ajouter une condition à un breakpoint :

```
condition [numero] [condition]
```

Attacher des commandes à un breakpoint :

```
commands [numero]
> [cmd1]
> [cmd2]
> end
```

## Exécution

Avancer d'une ligne :

```
step (=s)
next (=n) (ne rentre pas dans les fonctions)
```

Exécuter jusqu'à la fin d'un fonction :

```
finish
```

Exécuter jusqu'à un point :

```
until [emplacement]
```

## Information

Afficher le contenu d'une variable :

```
print [variable] (=p [variable])
```

Lister les variables :

```
info scope
```

Lister les breakpoints :

```
info breakpoints (=i b)
```

## Fichier `~/.gdbinit`

```
# Affichage indenté
set print pretty on

# Ne pas bloquer sur certains signaux
handle SIGPIPE nostop
handle SIG32 nostop
```

## Tricks

Ajouter du log sans recompiler :

```
break [emplacement]
commands [numero]
> print [variable]
> continue
> end
```

Avec cette commande gdb va s'arrêter sur `[emplacement]`, afficher le contenu de `[variable]` et reprendre l'exécution.

## Watch

```
p &v
watch *0x12345678
```
