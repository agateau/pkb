# ufw cheatsheet

## Show status

```
ufw status verbose
```

Use `verbose` to get the default incoming, outgoing and routed policies.

## Allow connections from IP address $SRC_IP to local port $PORT

```
ufw allow proto tcp from $SRC_IP to any port $PORT
```

`any` here says to connect on any destination IP address.

## Removing a rule

```
ufw status numbered
```

Use `numbered` to get rule numbers.

Then:

```
ufw delete $RULE_NUMBER
```

CAREFUL! Rules are re-numbered after deletion.

## Saving

Rules are stored in `/etc/ufw/user.rules` and `/etc/ufw/user6.rules`.
