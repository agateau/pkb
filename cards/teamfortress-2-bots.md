# TeamFortress 2

Setting up a LAN game with 2 players in the blue team, against bots.

To add bots:

```
tf_bot_add 7 blue
tf_bot_add 9 red
```

If bots do not move at the start of the game:

```
sv_cheats 1
nav_generate
```
