# Upload tarball to LP

- Click on the series (Foo project trunk series)
- Click on "Create release"
- Create a milestone (Use version number)
- Set date released to now
- Fill release notes
- Validate
- Click Add download file
  Description: "Source tarball"
