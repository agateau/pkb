# Kdenlive cheatsheet

## Sélection d'une partie d'un clip

- lancer la lecture
- i: place le point d'entrée
- o: place le point de sortie
- v: insère la zone entre les points d'entrée et de sortie dans la ligne temporelle

## Lecture du projet avec j, k et l

- l: lecture normale
- ll: lecture x2
- lll: lecture x3
- k: stop
- j: lecture en arrière x0.5
- jj: lecture en arrière x1
