# Integrating binaries in a Debian packages

## Copy files

```
tar xf ${package}_${version}.orig.tar.gz
cd $package-$version
cp $binaries
cd ..
tar czf ${package}_${version}+ag1.orig.tar.gz
cd $package-$version
cp $old_debian_dir .
```

## Recreate .dsc and .diff.gz

```
debuild -S
```
