# Mediawiki cheatsheet

## Headings

```
= Level 1 =
== Level 2 ==
=== Level 3 ===
```

## Links

Internal link:

```
[[Foo/Bar]]
[[Foo/Bar|Title]]
```

External link:

```
[http://example.com/foo Title]
```

## Character formatting

```
''italic''
'''bold'''
```

## Block formatting

```
<blockquote>Quoted text</blockquote>
```

## Lists

Bullets:

```
* foo
* bar
** nested foo
** nested bar
```

Numbered:

```
# foo
# bar
```

## Images

```
[[file:Foo_Bar.png]]
```

## techbase.kde.org specific stuff

### code

```
<syntaxhighlight lang="cpp-qt">
code
</syntaxhighlight>
```
