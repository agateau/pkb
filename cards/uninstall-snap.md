# Uninstall snap

## Remove snap

```
sudo apt autoremove --purge snapd gnome-software-plugin-snap firefox
rm -fr ~/snap
sudo apt-mark hold snapd
```
