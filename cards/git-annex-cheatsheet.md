# Git annex cheatsheet

## Setup

### Download release

<http://downloads.kitenet.net/git-annex/linux/current/>

### Creating a new repo on machine Foo

```
foo> cd repo
foo> git init
foo> git annex init "Foo"
```

### Clone repo on machine Bar

```
foo> ssh bar
```

```
bar> git clone git+ssh://foo/path/to/repo
bar> cd repo
bar> git annex init "Bar"
bar> exit
```

### Let Foo know about Bar

```
foo> cd repo
foo> git remote add bar git+ssh://bar/path/to/repo
foo> git annex describe bar "Bar"
```

## Usage

### Adding files

```
foo> cd repo
foo> cp /somewhere/bigfiles/jumbo.avi .
foo> git annex sync
```

```
bar> cd repo
bar> git annex get jumbo.avi
```

### Removing files from one host (to free space)

```
foo> cd repo
foo> git annex drop jumbo.avi
```

### Removing files definitely

```
foo> cd repo
foo> git annex drop jumbo.avi
foo> git rm jumbo.avi
foo> git annex sync
```

If it fails because jumbo.avi does not exist anywhere else:

```
foo> git annex move jumbo.avi --to bar
```

### Check status of a file

```
foo> git annex whereis jumbo.avi
```
