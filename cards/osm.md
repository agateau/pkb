# osm

## highway

- Nationale ou grosse départementale : primary
- Départementale : secondary
- Petite route entre communes : tertiary
- Petite route ne menant à rien : unclassified
- Rue résidentielle : residential
- Chemin non goudroné, accessible en voiture : track
- Chemin piétonier + vélo : path
- Chemin piétonier : footway

## barrier (node)

- Barrière qui s'ouvre : gate
- Barrière qui ne s'ouvre pas : stile
- Barrière qui laisse passer les vélos : cycle_barrier
- Bloc : block

## Place au bout d'une rue (node)

- highway=turning_circle

## Gite

- tourism=guest_house

## Terrain de boules

- landuse=recreation_ground
- sport=boules
