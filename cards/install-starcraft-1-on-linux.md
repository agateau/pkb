# Install StarCraft 1 on Linux

## Base install

- Install wine
- Mount starcraft iso
- cd to the iso mount dir
- wine ./install.exe

## To play over udp

- Download the 1.15.2 update from neo
- wine SC-1152.exe

## To play without the iso

Needs 1.15.2 or later.

- Starcraft:
    - cp /path/to/mount/install.exe /starcraft/install/dir/StarCraft.mpq
- Brood War:
    - cp /path/to/mount/install.exe /starcraft/install/dir/BroodWar.mpq
