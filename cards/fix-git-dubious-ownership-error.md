# Fix git dubious ownership error

For one repository:

```
git config --global --add safe.directory '/foo/bar'
```

For all repositories:

```
git config --global safe.directory '*'
```
