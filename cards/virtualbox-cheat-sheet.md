# VirtualBox cheat sheet

## Info

```
VBoxManage list vms

VBoxManage showvminfo $vmname
```

## Nat management

```
VBoxManage modifyvm $name --natpf1 "guestssh,tcp,,2222,,22"
VBoxManage modifyvm $name --natpf1 delete "guestssh"
```
