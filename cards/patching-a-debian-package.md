# Patching a Debian package

```
mkdir <package>
cd <package>
apt-get source <package>
```

When using bzr:

```
bzr clone <bzr-url>
rm -rf <package>-<version>/debian
cp ubuntu/debian <package>-<version>
```

```
cd <package>-<version>
git init
echo build >> .git/info/exclude
git add .
git ci -m 'Imported'
git gc --prune --aggressive
git checkout -b work
if [ -f debian/patches/series ] ; then
    echo '.pc' >> .git/info/exclude
    # git quiltimport --author 'kubuntu devs <kubuntu-devel@kubuntu.com>' --patches debian/patches
fi
```

## stgit

```
stg init
stg uncommit -t master -x
```

## Build

```
mkdir build
cd build
ubuntucmake ..
make
```

## Package

```
git checkout master
git checkout -b deb
git format-patch master..work -o debian/patches/
# Integrate with quilt

dch -i (x.y-z~ag1)
```

### Create binary package for internal tests

```
debuild -B
debuild -S -i'(build/|\.git|\.stgit-edit.txt|\.swp$)' -Ibuild -I.git
cd ..
dput <ppa> <name>...source.changes
```
