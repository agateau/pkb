# Reviewing code

Good read on the topic: <https://philbooth.me/blog/the-art-of-good-code-review>

Checklist:

- Security
- Observability
- Performance
- Robustness
- Complexity
- Readability
- Maintainability
- Over-engineering (YAGNI)
- Using existing abstractions correctly
- Naming stuff
- Comments
- Tests
- Doing one thing at a time (separate PRs)
- "Do I want to maintain this?"
