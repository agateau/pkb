# screencast

## Before

- Ensure popups and dialogs will fit in the video square
- Speak loud enough!
- Write a transcript of the cast and show it close to the recorded window
- Enable KWin mouse click effect to animate clicks
- Disable compositing (Alt+Shift+F12)
- Record a large file and re-encode after:
    - Container: mkv
    - Video: H264, superfast preset, constant rate factor: 6
    - Audio: uncompressed

## After

- Export as webm
- Check the resolution your video host prefers before resizing
