# GDX Interpolation curves

## Pow

```
               ****
             **
            *
           *
          *
        **
    ****
```

## Pow2In

```
           *
           *
          *
        **
    ****
```

## Pow2Out

```
        ****
      **
     *
    *
    *
```
