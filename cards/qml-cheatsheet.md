# QML cheatsheet

## JS Arrays

`property variant lst` cannot be modified in-place, only replaces:

```
// Bad
function() {
   lst.push("a");
}

// Good
function() {
  var newLst = lst;
  newLst.push("a");
  lst = newLst;
}
```

## JS Scope

Can refer to objects with an ID or to properties without prefixing with parent object:

```
Item {
  id: main
  Item {
    id: sub
  }
  property int value

  function f() {
    console.log(sub);
    console.log(value);
  }
}
```
