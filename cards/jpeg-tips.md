# JPEG tips

## In-place lossless optimization

```
jpegoptim -s -o *.jpg
```

## Reset orientation

```
exiftran -ai *.jpg
```
