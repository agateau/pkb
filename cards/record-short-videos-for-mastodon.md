# Record short videos for Mastodon

- Use SimpleScreenRecorder
- Settings:
    - Profile: High Quality Intermediate
    - WebM
    - VP8
    - Bitrate: 5000
    - CPU: 0

<https://www.paulox.net/2022/11/17/resize-a-video-with-ffmpeg-for-mastodon/>
