# Generating libgdx API doc

Get libgdx sources:

```
git clone https://github.com/libgdx/libgdx
cd libgdx
git checkout x.y.z
```

Generate doc in a doc/ dir:

```
javadoc -d doc -sourcepath gdx/src -subpackages com
```
