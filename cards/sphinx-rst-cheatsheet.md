# Sphinx rst cheatsheet

## Referencing elements

### Link to a function

    :func:`function_name`

### Link to a class

    :class:`MyClass`

### Link to a method

    :meth:`MyClass.my_method`

### Refer to a function argument

    ``arg_name``

Example:

    def get(key):
        """
        Returns the value for ``key``.
        """

## Links

- [Referencing Python things](https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#cross-referencing-python-objects)

- [reStructuredTextPrimer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)

Google examples:

- <https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html>
- <https://google.github.io/styleguide/pyguide.html>
