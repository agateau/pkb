# Disable fullscreen/immersive popup on Android

```
adb shell settings put secure immersive_mode_confirmations confirmed
```

Source: <https://stackoverflow.com/questions/62471349/espresso-test-fails-due-to-android-message-viewing-full-screen-to-exit-swipe>
