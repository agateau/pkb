# Concat images

montage -geometry +2+2 *.png out.png

-geometry +2+2 => 2 margin pixels, no resize
-tile $1x$2 => adjust grid size, but montage is usually smart enough
