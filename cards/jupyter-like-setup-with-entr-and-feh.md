# Jupyter-like setup with entr and feh

## Auto-update plot image when working on plotting script

First, run the script once to create the image.

Second, start `feh` on the image: `feh path/to/plot.png`.

Now run the script via `entr` and tell `feh` to refresh itself. The key is to run the script with the shell (`-s` option in `entr`) and tell `feh` to refresh itself by sending it a `SIGUSR1` signal.

```
echo scripts/tograph.py | entr -s "python scripts/tograph.py out.csv out.png && killall -s SIGUSR1 feh"
```
