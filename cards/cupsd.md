# cupsd

## Access from LAN

```
Listen *:631
```

Add `Allow From @LOCAL` to all `<location>`.

## No TLS

Add `DefaultEncryption Never`

## Allow a user to administrate

Add user to unix group "lpadmin" with:

```
usermod -aG lpadmin <user>
```
