# Reinstall a dev environment

## Backup

```
mkdir backup
cd backup
dpkg --get-selections | awk '$2 == "install" { print $1 }' > debs.lst
crontab -l > crontab
sudo rsync -aP /etc .
sudo cp -R /root/.ssh root-ssh
```

- Check /opt
- Check /usr/local (find /usr/local -type f)
- Export this task to a text file

## Install

- Install from CD/USB key
- Install apps

```
sudo apt-get install etckeeper

sudo apt-get install --no-install-recommends -y \
    git-core git-gui gitk tig \
    vim-gtk3 exuberant-ctags zsh patchutils g++ \
    htop ncdu nethogs \
    python3 python3-pip ipython3 python3-setuptools libpython3-dev \
    ccache cmake ninja-build make \
    qtbase5-dev python3-pyqt5 python3-pyqt5.qtwebkit \
    openssh-server openssh-client curl tmux \
    vlc clementine yakuake inkscape gimp quassel-client \
    dmenu fzf kruler \
    pandoc groff moreutils

sudo -H pip3 install ptpython pew youtube-dl
```

- Setup SSH
- Clone etc repo and setup

```
etc/ln-dot-files
```

- Set default shell

```
chsh -s /bin/zsh $USER
```

- Check backup/debs.lst for additional software to install
- Restore /opt and /usr/local content

Install my apps:

- yokadi
- kapti
- dsched
- deveba
- devo
- mup
- colorpick
- sfxr-qt
- nanonote
- pvmetatools
- rstblog
- rstblog-tools
- git-bonsai
- git-uff

Install extra apps:

- fd
- ripgrep
- Android Studio
- avidemux
- unison
- fzf

## Post-install

- Update ssh keys on relevant servers
