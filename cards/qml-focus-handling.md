# QML focus handling

One cannot set tab navigation on an item inside a FocusScope.

Example:

    Item {
        function doSomething() {
            foo.forceActiveFocus(); // Doesn't work
        }

        FocusScope {
            Item {
                id: foo
            }
        }
    }

A FocusScope automatically passes focus to the last child with `focus=true`.

Alternative to FocusScope:

    Item {
        onActiveFocusChanged: {
            if (activeFocus) {
                anotherItem.forceActiveFocus();
            }
        }
    }
