# Adjust timestamp for a video

    avconv -i input.mov \
        -metadata "creation_time=2012-12-22 10:50:02" \
        -codec copy \
        output.mov
