# Create gpx track in Viking

- Add map layer (the second one)
- Add track layer
- Click on "Edit route" (orange +)
- Draw track
- Double-click to place final point
- Add start timestamp:
    - Click on "Edit trackpoint"
    - Click on "Timestamp"
    - Enter timestamp
- Do the same for end timestamp
- Export the layer as gpx
- Run gpxinterp on the gpx
