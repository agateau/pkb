# pbuilder cheatsheet

## Build package

```
pdebuild
```

or

```
bzr bd --builder=pdebuild
```

## Do permanent changes

```
sudo pbuilder login --save-after-login
```
