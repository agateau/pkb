# Wipe / clean a Linux laptop

## Directories to rm

```
rm -rf ~/.ssh ~/.config ~/.local ~/.cache ~/.mozilla ~/me_src ~/etc ~/bin ~/src ~/wip
```

Or just delete all:

```
rm -rf *
rm -rf .*
```

Using tools like `shred` is useless on SSD, because sequential writes on the same file do not overwrite each others (see <https://unix.stackexchange.com/a/593340>).

## Change password

```
passwd $password
```

## Change encryption key

```
sudo cryptsetup luksChangeKey $partition
```
