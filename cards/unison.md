# Unison

## Basic config

In `~/.unison/common`:

```
copythreshold = 5000
perms = 0
dontchmod = true

# Ignore some backup files
ignore = Name .*.lock
ignore = Name .*.sw?
ignore = Name .*~
ignore = Name *~
```

To create a "foo" profile, create `~/.unison/foo.prf`:

```
root = /home/bob/doc
root = ssh://example.com//home/bob/doc
include common
```

## Manual

<https://www.cis.upenn.edu/~bcpierce/unison/download/releases/stable/unison-manual.html>
