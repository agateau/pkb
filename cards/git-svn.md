# git-svn

## Init repository

```
url=https://gateau@svn.kde.org/home/kde/branches/extragear/kde3/libs
svn info $url
...
Last Changed Rev : 671034
...

mkdir libs
cd libs
git-svn init $url

# Fetch from this revision
# Use the value of "Last Changed Rev", do not use -rHEAD 
git-svn fetch -r671034

# If you selected an older revision, fetch all revisions
git-svn fetch
git-svn rebase
```

## Hack

```
git-status
git-diff
git-commit -a
git-checkout some/file (<=> svn revert some/file)
```

## Update from svn

```
git-svn rebase
```

## Commit to svn

```
git-svn dcommit
```
