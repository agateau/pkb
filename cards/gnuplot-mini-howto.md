# Gnuplot mini-howto

Create a space-separated file (lines starting with '#' are ignored)

Plot the 4th column of date from file.dat, using the 1st column as X axis:

    plot "file.dat" using 1:4

Use line and points instead of just poinst:

    plot "file.dat" using 1:4 with lines

Plot 4th and 5th column

    plot "file.dat" using 1:4 with lines, "file.dat" using 1:5 with lines

Define a line style and change color of 2nd graph:

    set style line 1 linecolor rgb "blue"
    plot "file.dat" using 1:4 with lines, "file.dat" using 1:5 linestyle 1 with lines
