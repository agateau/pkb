# dma install (Dragonfly Mail Agent)

## Install

```
apt-get install dma
```

## Configure

`/etc/dma/auth.conf`:

```
gateau@fastmail.fm|mail.messagingengine.com:<password>
```

`/etc/dma/dma.conf`:

```
SMARTHOST mail.messagingengine.com
PORT 465
AUTHPATH /etc/dma/auth.conf
SECURETRANSFER
INSECURE
MAILNAME /etc/mailname
MASQUERADE gateau.info
```

`/etc/aliases`:

```
*: aurelien@gateau.org
```
