# Debugging link errors

Check library name is in ld command-line
Check library path is in ld command-line
Check symbols are in linked library (objdump -tT --demangle .so file)
Check there is no old version of the library in

- same path (libfoo.so.1.0, libfoo.so.1.1)
- other paths ($HOME/opt/lib/ vs /usr/lib)
