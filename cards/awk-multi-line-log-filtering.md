# Awk multi-line log filtering

Starting from a file with content like this:

```
# Getting links from: https://www.genymotion.com/
# ├───OK─── https://www.genymotion.com/wp-content/themes/genymobile/img/genymobile-with-motion.svg
# ├───OK─── https://www.genymotion.com/cloud/
# ├───OK─── https://www.genymotion.com/desktop/
# ├───OK─── https://www.genymotion.com/develop-your-app/
#
# Getting links from: https://www.genymotion.com/develop-your-app/
# ├───OK─── https://youtube.com/embed/DYFuh6G0Obs?autoplay=1
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2018/06/Simulates-abilities.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2018/06/Simulates-context-actions.png
# ├─BROKEN─ https://docs.genymotion.com/Content/Home.htm (HTTP_404)
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2018/06/work-fast-and-better.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2018/06/ic-scale-test-cloud.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2018/06/ic-run-your-app-1.png
# Finished! 141 links found. 134 excluded. 1 broken.
#
# Getting links from: https://www.genymotion.com/test-automation/
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2018/06/ic-power-of-test.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2018/06/ic-plug-genymotion.png
#
# Getting links from: https://www.genymotion.com/help/desktop/faq/
# ├───OK─── https://www.genymotion.com/account/login
# ├───OK─── https://www.genymotion.com/pricing-and-licensing/
# ├───OK─── https://www.virtualbox.org/wiki/Download_Old_Builds_5_2
# ├───OK─── https://www.virtualbox.org/manual/ch12.html#ts_lin-hosts
# ├───OK─── https://download.virtualbox.org/virtualbox/6.0.4/VirtualBox-6.0.4-128413-Win.exe
# ├───OK─── https://download.virtualbox.org/virtualbox/6.0.4/VirtualBox-6.0.4-128413-OSX.dmg
# ├───OK─── http://rpmfusion.org/Configuration/
# ├───OK─── http://rpmfusion.org/Howto/VirtualBox
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2016/11/FAQ_Host-Only-04.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2016/11/FAQ_Host-Only-04-300x86.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2016/11/FAQ_Host-Only-05.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2016/11/FAQ_Host-Only-05-300x168.png
# ├───OK─── https://www.genymotion.com/wp-content/uploads/2016/11/Remove_host_only_network_icon04.png
# ├───OK─── https://www.virtualbox.org/wiki/Downloads
# ├─BROKEN─ http://osxdaily.com/2018/12/31/install-run-virtualbox-macos-install-kernel-fails/ (HTTP_403)
# ├───OK─── https://www.genymotion.com/download
# ├───OK─── http://support.microsoft.com/kb/889768
# ├─BROKEN─ http://support.apple.com/kb/HT1652 (HTTP_404)
# ├───OK─── http://www.hanselman.com/blog/SwitchEasilyBetweenVirtualBoxAndHyperVWithABCDEditBootEntryInWindows81.aspx
# ├───OK─── https://finsterbt.com/switch-between-hyper-v-and-virtualbox-on-windows-10/
# ├───OK─── http://cntlm.sourceforge.net/
# ├───OK─── http://ntlmaps.sourceforge.net/
```

If there are any BROKEN link, print the "Getting links" header and all BROKEN links.

```
awk '
$0 ~ "Getting links" {
    header_to_print = $0;
}
$0 ~ "BROKEN" {
    if (header_to_print) {
        printf("\n%s\n", header_to_print);
        header_to_print = "";
    }
    print $0;
}
' report-www.genymotion.com.log
```
