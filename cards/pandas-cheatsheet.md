# Pandas cheatsheet

## Import csv

```python
import pandas as pd

df = pd.read_csv("/path/to/csv")

## To set headers
df = pd.read_csv("/path/to/csv", names=["col1", "col2"])
```

## Sort data

```
df.sort_values(by=["col1", "col2"], ascending=False)
```

## Creating a dataframe

It can be simpler to group data by columns (aka series) in a `defaultdict(list)` and create the dataframe from it. Something like this:

```
dct = defaultdict(list)

for foo, bar in data:
    dct["foo"].append(foo)
    dct["bar"].append(bar)

df = DataFrame.from_dict(dct)
```

## References

<https://devarea.com/pandas-for-sql-users/>
<https://pandas.pydata.org/pandas-docs/stable/getting_started/comparison/comparison_with_sql.html>
