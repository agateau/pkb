# Downloading from openid-secured sites with wget

Order of cookies is from most specific path to most generic (ie /login before /)

wget --no-cookies \
 --header "Cookie: mpopenid.last_user=<https://login.launchpad.net/+id/y4whdGQ>; pysid=80736d2c5c542b4e214573911b0a9dd2" \
 <url>
