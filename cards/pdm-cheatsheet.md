# pdm cheatsheet

## Update lock, including any new dev dependencies

```
pdm lock -d
```

## Interaction with tox

It seems tox cannot install pdm dev dependencies, so dev dependencies must be duplicated in tox `deps =` configuration key.
