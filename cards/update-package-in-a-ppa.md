# Update package in a PPA

## First time setup

### Local

- Install necessary tools:

```
sudo apt-get install devscripts debuild
```

- Create a packaging dir:

```
mkdir ~/packaging
```

- Create a GPG key.

- Optional: set `DEBEMAIL` environment variable to the email address you want to
appear in the package changelog.

### Launchpad

- Create a Launchpad account
- Add a GPG key
- Create a PPA

## Download existing source package

```
cd ~/packaging/
mkdir colibri
cd colibri
apt-get source colibri
```

## Copy new tarball

```
cp /dir/where/upstream/tarball/is/colibri-0.3.0.tar.bz2 .
mv colibri-0.3.0.tar.bz2 colibri_0.3.0.orig.tar.bz2
```

## Update source package

```
uupdate ../colibri_0.3.0.orig.tar.bz2
```

This command creates a dir named ~/packaging/colibri/colibri-0.3.0. Change to it:

```
cd ../colibri-0.3.0
```

## Update changelog

Edit `debian/changelog`:

- Add a "~ppa1" to the version number
- Set a correct release name
- Update changelog details for this new version if necessary

`debian/changelog` before:

```
colibri (0.3.0-0ubuntu1) UNRELEASED; urgency=low

  * New upstream release

 -- Aurélien Gâteau <agateau@ubuntu.com>  Wed, 15 May 2013 10:30:30 +0200
```

`debian/changelog` after:

```
colibri (0.3.0-0ubuntu1~ppa1) raring; urgency=low

  * New upstream release

 -- Aurélien Gâteau <agateau@ubuntu.com>  Wed, 15 May 2013 10:30:30 +0200
```

## Build a binary package

```
debuild -b -uc -us
```

`-b` means binary only, `-uc -us` means no gpg signatures. This is useful to quickly test your package.

## Install and test the binary package

```
sudo dpkg -i ../colibri_0.3.0-0ubuntu1~ppa1_amd64.deb
```

Test it works.

## Build a source package

```
debuild -S
```

## Upload it to a PPA

```
dput ppa:agateau/colibri ../colibri_0.3.0-0ubuntu1~ppa1_source.changes
```
