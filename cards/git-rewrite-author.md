# Git rewrite author

Rewrite the author of all commits, keep tags and commit dates.

## Using `git filter-repo`

Install `git filter-repo` (`clyde install git-filter-repo`)

Create a mailmap file like this:

```
New Name <new@email> <old@email>
```

Run `git filter-repo --mailmap path/to/mailmap`.

## Old version, using deprecated `git filter-branch`

```
git filter-branch --force \
    --commit-filter 'GIT_AUTHOR_EMAIL=mail@agateau.com GIT_COMMITTER_EMAIL=$GIT_AUTHOR_EMAIL git commit-tree "$@"' \
    --tag-name-filter cat
```

## Using `git rebase` (affects only a series of commits)

Set the right author info in the repo.

Run:

```
git rebase -i <commit_id> --exec 'git commit --amend --no-edit --reset-author'
```
