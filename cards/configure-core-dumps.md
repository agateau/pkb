# Configure core dumps

## Create all core dumps in /var/crash, include user ID and program name in the filename.

- %u uid
- %e filename
- %p pid

```
sudo sysctl -w kernel.core_pattern=/var/crash/%u.%e.%p.core
```
