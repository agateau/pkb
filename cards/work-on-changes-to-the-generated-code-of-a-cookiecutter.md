# Work on changes to the generated code of a cookiecutter

- Run cookiecutter to create the work dir (`WORK_DIR`)
- Create a git repository:
    cd $WORK_DIR
    git init
    git add .
    git commit -m 'Import'
- Tag first commit
    git tag start
- Make changes, create commits

When it is time to merge the changes back:

- Generate patches:
    git format-patch start
- Go back to cookiecutter repository
    cd $COOKIECUTTER_DIR
- Apply patches
    git am --directory '{{cookiecutter.project_slug}}' $WORK_DIR/*.patch
