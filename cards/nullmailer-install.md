# nullmailer install

    apt-get install nullmailer

Remote to use with fastmail:

    mail.messagingengine.com smtp --port=465 --user=XXXXXX@fastmail.fm --pass=YYYYYY --starttls --ssl

remote to use with Gandi:

    mail.gandi.net smtp --port=587 --user=XXXXXX@gateau.info --pass=YYYYYY --starttls

Forward local mails: write recipient in /etc/nullmailer/adminaddr

For desktop machines:

    cd /etc/nullmailer
    rm defaultdomain
    echo 'gateau.info' > defaulthost
