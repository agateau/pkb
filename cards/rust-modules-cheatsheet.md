# Rust modules cheatsheet

## Creating, importing

Create a module:

```
# foo.rs

mod foo {

    pub fn bla() {
    }

}
```

Import a module:

```
// bar.rs

mod foo;
```

Import a module, make it accessible for others

```
// bar.rs

pub mod foo;

// Others can `mod bar;` and then call `bar.foo.bla()`.
```

## Import from an external create

```
extern crate baz;

baz.some_baz_function();
```

## Shorten paths to module content

```
mod foo;
```

foo.bla() must be called explicitly:

```
foo.bla();
```

Instead:

```
use foo::bla;
```

It's now possible to do:

```
bla();
```

## Import a module from another module, within the same crate

```
// thing.rs

use crate::foo;

foo.bla();

```

```
// lib.rs or main.rs
mod foo;
```

## Make code from your own app available for integration tests

Assuming the app crate is called "myapp", create src/lib.rs

```
// lib.rs

pub mod foo;
```

Now in integration test code:

```
extern crate myapp;

myapp.foo.bla();
```
