# libgdx layout tips

## layout()

- DO: resize and reposition children
- DONT: resize or reposition ourselves
- DONT: call `invalidateHierarchy()`

## After resizing ourself

Call `invalidateHierarchy()`.

## After making changes which require children to be layouted

Call `invalidate()`.

## Misc

To react to an actor changing position or size: reimplement `Actor.positionChanged()` or `Actor.sizeChanged()`.
