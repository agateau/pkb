# Workaround kdenlive wrong color rendering

/usr/bin/melt clara.kdenlive -profile dv_pal progress=1 -consumer avformat:/tmp/clara.mp4 rescale=bicubic f=mp4 acodec=aac ab=96k ar=44100 vcodec=libx264 minrate=0 vb=600k g=250 bf=3 s=512x384 mbd=2 trellis=1 mv4=1 subq=7 qmin=10 qcomp=0,6 qdiff=4 qmax=51

What matters is "rescale=bicubic", see:

<http://sourceforge.net/tracker/?func=detail&aid=3483629&group_id=96039&atid=613414>
