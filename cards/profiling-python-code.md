# Profiling Python code

## Performance

### py-spy

<https://github.com/benfred/py-spy>

```
py-spy record -o output.svg -- python <app.py>
```

### cProfile

Profile code:

```
python -m cProfile -o out.profile <module-to-test>
```

#### Profiling threaded code

Threads are not profiled. Need to wrap the call with a wrapper, for example:

```python
def profile_wrapper(fcn: Callable) -> Callable:
    """Wraps a function call to profile it. Useful to profile a function called from
    another thread"""

    def inner(fcn: Callable, *args: Any, **kwargs: Dict[str, Any]) -> Any:
        pr = cProfile.Profile()
        try:
            pr.enable()
            return fcn(*args, **kwargs)
        finally:
            pr.disable()
            pr.dump_stats(f"out-{threading.current_thread().ident}.profile")

    return functools.partial(inner, fcn)
```

#### Visualizing

##### Using graphviz

Install gprof2dot:

```
pip install gprof2dot
```

Generate graph:

```
gprof2dot -f pstats out.profile | dot -Tpng | feh -
```

##### Using snakeviz

```
pip install snakeviz

snakeviz out.profile
```

## Memory

### filprofiler

<https://pythonspeed.com/fil/docs/index.html>
