# Change comment color in Squish IDE

- Open Preferences
- Go to PyDev > Editor
- Select "Comments" in "Appearance color options"
